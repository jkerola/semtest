#!/bin/bash
set -e
export VERSION=$1
# print build version
echo "=> Build version: ${VERSION}"

# Export environment variables for later jobs
touch build.env
echo "APP_VER=${VERSION}" >> build.env

# Update pubspec with latest version number
echo "=> Update pubspec.yaml"
cider version ${VERSION}
echo "=> Fetch packages"
flutter pub get

# Build android application
export NAME_ANDROID="snh-${VERSION}-android.apk"
echo "=> Build APK: ${NAME_ANDROID}"
echo "NAME_ANDROID=${NAME_ANDROID}" >> build.env
flutter build apk --release

# Build web application
export NAME_WEB="snh-${VERSION}-web.zip"
echo "=> Build web bundle: ${NAME_WEB}"
echo "NAME_WEB=${NAME_WEB}" >> build.env
flutter build web --release

# Zip builds for convenience
echo "=> Zip build"
zip -r builds.zip build/web build/app/outputs/flutter-apk/app-release.apk

echo "Succesfully built v.${VERSION}!"
