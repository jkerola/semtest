# Please see ci/build-app for directions to exported names
variables:
  BASE_IMAGE: $CI_REGISTRY/$CI_PROJECT_PATH/base-image
  APK_PATH: build/app/outputs/flutter-apk/app-release.apk
  PACKAGE_REGISTRY: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/semtest/$APP_VER
  PKG_ANDROID: $PACKAGE_REGISTRY/$NAME_ANDROID
  PKG_WEB: $PACKAGE_REGISTRY/$NAME_WEB

stages:
  - test
  - pre-build
  - build
  - upload
  - release

# see ci/Dockerfile for details
build-base-image:
  image: docker:stable
  stage: pre-build
  tags:
    - office-runner
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      changes:
        - ci/Dockerfile
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  script:
    - docker pull $BASE_IMAGE:latest || true
    - docker build --cache-from $BASE_IMAGE:latest --tag $BASE_IMAGE:latest -f ./ci/Dockerfile .
    - docker push $BASE_IMAGE:latest

unit-tests:
  image: $BASE_IMAGE:latest
  stage: test
  tags:
    - office-runner
  rules:
    # should run on every merge request branch, but not main
    - if: $CI_COMMIT_BRANCH != "main"
  before_script:
    - flutter pub global activate junitreport
    - flutter pub get
  script:
    - flutter test --machine | tojunit --output testreport.xml
  artifacts:
    when: always
    expire_in: 3 days
    reports:
      junit: testreport.xml

build-app:
  image: $BASE_IMAGE:latest
  stage: build
  tags:
    - office-runner
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  variables:
    GIT_STRATEGY: clone
  before_script:
    # see .releaserc.yaml for plugin configuration details
    - "npm install -g semantic-release @semantic-release/exec @semantic-release/changelog"
    - flutter pub global activate cider
    - chmod +x ./ci/build-app.sh
  script:
    - semantic-release
  artifacts:
    when: on_success
    expire_in: 30 minutes
    reports:
      dotenv: build.env
    paths:
      # Keep changelog for release notes
      - "docs/CHANGELOG.md"
      # see ci/build-app.sh for details
      - "builds.zip"

upload-build:
  stage: upload
  tags:
    - office-runner
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  dependencies:
    - build-app
  before_script:
    # Default image is alpine:latest when no image is specified
    - apk add curl zip
    - unzip builds.zip
    # For builds with many files, compressing is encouraged
    - zip -r $NAME_WEB build/web
  script:
    - echo "=> Uploading releases"
    # Push built binaries/bundles to gitlab package registry
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $APK_PATH $PKG_ANDROID --fail'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $NAME_WEB $PKG_WEB --fail'

release-app:
  stage: release
  # Used for release-cli
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  dependencies:
    - build-app
    - upload-build
  tags:
    - office-runner
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  script:
    - echo "=> Creating release $APP_VER"
  release:
    # Configure release details here
    tag_name: $APP_VER
    name: "SNH-App $APP_VER"
    description: "docs/CHANGELOG.md"
    assets:
      # Since we cannot upload files directly, we include links to the package registry
      links:
        - name: "SNH-App v$APP_VER for Android"
          url: $PKG_ANDROID
          filepath: "/android/$NAME_ANDROID"
          link_type: package
        - name: "SNH-App v$APP_VER for Web"
          url: $PKG_WEB
          filepath: "/web/$NAME_WEB"
          link_type: package
